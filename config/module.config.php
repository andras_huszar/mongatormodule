<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'MongatorModule\Console' => 'MongatorModule\Controller\ConsoleController',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
                'mongator_module_mondator_process' => array(
                    'options' => array(
                        'route'    => 'mongator mondator process',
                        'defaults' => array(
                            'controller' => 'MongatorModule\Console',
                            'action'     => 'mondator-process',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'mongator' => array(
        'default_connection' => 'default',
    ),
    'service_manager' => array(
        'factories' => array(
            'Mongator\Mongator' => 'MongatorModule\Service\MongatorFactory',
            'Mongator\FilesystemCache' => 'MongatorModule\Service\FilesystemCacheFactory',
        ),
    ),
);
