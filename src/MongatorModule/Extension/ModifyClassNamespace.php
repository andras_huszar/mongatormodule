<?php

namespace MongatorModule\Extension;

use Mandango\Mondator\Extension;
use Mandango\Mondator\Definition\Definition;
use Mandango\Mondator\Definition\Property;
use Zend\Server\Method\Prototype;
use Mandango\Mondator\Definition\Method;

class ModifyClassNamespace extends Extension
{
    protected $oldClass;

    protected function doClassProcess()
    {
        $this->oldClass = $this->definitions['document']->getClass();
        $this->modifyNamespace($this->definitions['document'], 'entity');

        if (!$this->configClass['isEmbedded']) {
            $this->modifyNamespace($this->definitions['repository'], 'repository');
            $this->modifyNamespace($this->definitions['query'], 'query');

            $this->replaceClassInMethod($this->definitions['repository_base']->getMethodByName('__construct'));
        }
    }

    protected function modifyNamespace(Definition $definition, $directory)
    {
        $definition->setClass($definition->getNamespace() . '\\' .
            ucfirst($directory) . '\\' . $definition->getClassName());
    }

    protected function replaceClassInMethod(Method $method)
    {
        $newClass = $this->definitions['document']->getClass();
    	$method->setCode(str_replace($this->oldClass, $newClass, $method->getCode()));
    }
}