<?php
namespace MongatorModule\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Mandango\Mondator\Mondator;
use Mongator\Extension\Core as MongatorCore;
use MongatorModule\Extension\ModifyClassNamespace;

class ConsoleController extends AbstractActionController
{
    public function mondatorProcessAction()
    {
        // Config
        $config = $this->getServiceLocator()->get('Config');
        $config = $config['mongator'];

        // Start Mondator
        $mondator = new Mondator();

        // Assign the config classes
        if (is_array($config['mappings'])) {
            $mondator->setConfigClasses($config['mappings']);
        }

        // Assign extensions
        $mondator->setExtensions(array(
            new MongatorCore(array(
                'metadata_factory_class' => $config['metadata_factory_class'],
                'metadata_factory_output' => $config['metadata_factory_output'],
                'default_output' => $config['default_output'],
            )),
            new ModifyClassNamespace(),
        ));

        // Process
        $mondator->process();
    }
}
