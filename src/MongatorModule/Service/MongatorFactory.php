<?php

namespace MongatorModule\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use WooBackendModel\Mapping\MetadataFactory;
use Mongator\Mongator;
use Mongator\Connection;

class MongatorFactory implements FactoryInterface
{
    protected $configKey = 'mongator';

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        // Config
        $config = $services->get('Config');
        $config = isset($config[$this->configKey]) ? $config[$this->configKey] : array();

        // Metadata factory
        $metadata = new $config['metadata_factory_class']();

        // Set caching
        $cache = $services->get('Mongator\FilesystemCache');

        // Init Mandango
        $mongator = new Mongator($metadata, $cache);

        // Set connections
        $connections = array();
        foreach ($config['connections'] as $name => $connection) {
            $connections[$name] = new Connection($connection['server'], $connection['database']);
        }
        $mongator->setConnections($connections);

        // Set default connection
        $mongator->setDefaultConnectionName($config['default_connection']);

        return $mongator;
    }
}