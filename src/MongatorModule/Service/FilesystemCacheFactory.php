<?php

namespace MongatorModule\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Mongator\Cache\FilesystemCache;

class FilesystemCacheFactory implements FactoryInterface
{
    protected $configKey = 'mongator';

    /**
     * @see \Zend\ServiceManager\FactoryInterface::createService()
     */
    public function createService(ServiceLocatorInterface $services)
    {
        $config = $services->get('Config');
        $config = isset($config[$this->configKey]) ? $config[$this->configKey] : array();

        return new FilesystemCache($config['cache']['filesystem']['path']);
    }
}