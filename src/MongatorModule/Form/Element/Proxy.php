<?php
namespace MandangoModule\Form\Element;

use RuntimeException;
use Mandango\Mandango;

class Proxy {
    /**
     * @var array
     */
    protected $objects;

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @var array
     */
    protected $valueOptions = array();

    /**
     * @var Mandango
     */
    protected $mandango;

    public function setOptions($options) {
        if (isset($options['mandango'])) {
            $this->setMandango($options['mandango']);
        }

        if (isset($options['model_class'])) {
            $this->setModelClass($options['model_class']);
        }
    }

    /**
     * Returns value options.
     * Trying to load it if not loaded.
     *
     * @return array
     */
    public function getValueOptions() {
        if (empty($this->valueOptions)) {
            $this->loadValueOptions();
        }
        return $this->valueOptions;
    }

    /**
     * @return array
     */
    public function getObjects() {
        $this->loadObjects();
        return $this->objects;
    }

    /**
     * Set Mandango
     *
     * @param Mandango $mandango
     * @return $this
     */
    public function setMandango(Mandango $mandango) {
        $this->mandango = $mandango;
        return $this;
    }

    /**
     * Get Mandango
     *
     * @return Mandango
     */
    public function getMandango() {
        return $this->mandango;
    }

    /**
     * Set the FQCN of the model class
     *
     * @param  string $model
     * @return Proxy
     */
    public function setModelClass($modelClass) {
        $this->modelClass = $modelClass;
        return $this;
    }

    /**
     * Get the model class
     *
     * @return string
     */
    public function getModelClass() {
        return $this->modelClass;
    }

    /**
     * @param  $value
     * @return array|mixed|object
     * @throws \RuntimeException
     */
    public function getValue($value) {
        if (!($mandango = $this->getMandango())) {
            throw new RuntimeException('No Mandango was set');
        }

        if (!($modelClass = $this->getModelClass())) {
            throw new RuntimeException('No model class was set');
        }

        if (is_object($value)) {
            if ($value instanceof \Mandango\Group\ReferenceGroup) {
                $saved = $value->getSaved();
                $data  = array();
                foreach($saved as $object) {
                    $data[] = $object->getId();
                }
                $value = $data;
            } else {
                //$value = $value->getId();
            }
        }

        return $value;
    }

    /**
     * Load objects
     *
     * @return void
     */
    protected function loadObjects() {
        if (!empty($this->objects)) {
            return;
        }

        if (!($mandango = $this->getMandango())) {
            throw new RuntimeException('No Mandango was set');
        }

        if (!($modelClass = $this->getModelClass())) {
            throw new RuntimeException('No model class was set');
        }

        $repository = $mandango->getRepository($modelClass);

        $this->objects = $repository->createQuery()->all();
    }

    /**
     * Load value options
     *
     * @throws \RuntimeException
     * @return void
     */
    protected function loadValueOptions() {
        if (!($mandango = $this->getMandango())) {
            throw new RuntimeException('No Mandango was set');
        }

        if (!($modelClass = $this->getModelClass())) {
            throw new RuntimeException('No model class was set');
        }

        $objects = $this->getObjects();
        $options = array();

        if (empty($objects)) {
            $options[''] = '';
        } else {
            foreach ($objects as $key => $object) {
                $options[] = array(
                    'label' => $object->getTitle(),
                    'value' => $object->getId(),
                );
            }
        }

        $this->valueOptions = $options;
    }
}
